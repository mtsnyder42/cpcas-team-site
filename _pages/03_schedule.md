---
layout: page
title: Release Schedule
permalink: /schedule
---

### __MAR 16__: Home Page Complete  
### __MAR 30__: All Static Content Complete  
### __APR 03__: ITERATION #2 - Intermediate Release  
### __APR 10__: Dynamic content finished  
### __APR 27__: ITERATION #3 - Minimum Viable Product Release  
  
  
  
