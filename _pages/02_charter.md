---
layout: page
title: Project Charter
permalink: /charter
---
<a name="top"></a>
[Charter Questions and Answers](#QandA)  
[Elevator Statement](#elevator)  
[Measures of Success and Sliders](#measures)  
  
  -----------------------------------------
<a name="QandA"></a>
## Charter Questions and Answers
#### Who are we? (Students, instructor, and customers)  
Instructor: Dr. Gerald C. Gannod  
Client: Cookeville and Putnam County Animal Shelter  
Students:  Edmond Hurst, Jacob Franz, Michael Snyder  

#### What are our values? (All)  
Clarity, honesty, professionalism, determination, ethicality, and customer-focus  

#### How will we communicate? (All)  
Discord, Slack, and TNTech email  

#### How will we make decisions? (All)  
By group vote with explanations  

#### How will we handle conflict? (Teams)  
By voting on the matter, no possibility of tie with 3 members

#### How will we run our meetings? (Teams)  
Effeciently

#### When will we meet?  
Thursday at 3:00m in Bruner 406  

#### How often will we meet?  
Every week and as necessary to accomplish goals on time within reasonable consideration of each member's existing schedule  

#### How will we manage our work? (All)  
We will outline tasks and assign them based on member's abilities and each task's difficulty

#### What systems should be used to share files? (Google Drive)  
Discord and Bitbucket  

#### How will keep track of our progress? (Trello.com)  
Trello.com  

#### How will we have fun?  
By being informal and joking around as we get work done

#### How will we improve our processes?  
By analyzing how quick and efficient we are  

#### Retrospectives? How often?  
After every iteration and sub-goal as indicated on release plan  

#### What is our working agreement?  
Every member completes the tasks assigned to them
  
  
  
  
  
<a name="elevator"></a>
## Elevator Statement 
For: For Cookeville and Putnam County Animal Shelter (CPCAS) and surrounding community  
Who: Needs information to be broadcasted about the CPCAS (contact info, hours, location, etc) to the public  
The: CPCAS homepage  
Is A: website that will relay the desired information  
Unlike: the current website  
Our Solution: will be more structured, include more information, and will be accurate  
  
  
  
  
  
<a name="measures"></a>
## Measures of Success and Sliders  
Time......|---|---|---| 4 |  
Scope....|---| 2 |---|---|  
Quality...|---|---| 3 |---|  
Cost......| 1 |---|---|---|  


[Back to top of page](#top)
