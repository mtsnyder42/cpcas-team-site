---
layout: page
title: User Stories
permalink: /stories
---
## Current user stories can be found on [Trello](https://trello.com/b/r0jqPRq1/cpcas)
  
  -----------------------------------------
### Must  
* As the CPCAS, I want our shelter information hosted online  
* As a local person I need to know the location of the shelter to adopt, visit, or volunteer  
  

### Should  
* As the CPCAS, I want to post lost animals  
* As the CPCAS, I want to post found animals  
* As an internet visitor, I want to be able to donate online  
* As a local person, I want to view adoptable pets so that I can choose one to adoptable  
* As a pet owner, I want to view pets found by the shelter  
  

### Could  
* As a local person I want to see how to adopt and what fees I will need to pay  
* As the CPCAS, I want to share successful adoption stories  
* As the CPCAS, I want to broadcast events we are doing  
  

### Won't  
* As a volunteer, I want to clock in and clock out  
* As a volunteer, I want to schedule my self to work for the shelter  
* As a manger, I want to look at volunteer hours  
* As a manger, I want to see where volunteers worked at the shelter  
  
